# capabilities
DEVICE_NAME_ANDROID = 'Android device'
APP_PACKAGE_ANDROID = 'com.android.chrome'
PLATFORM_NAME_ANDROID = 'Android'
DEVICE_NAME_IOS = ""
PLATFORM_NAME_IOS = ""
PLATFORM_VERSION_IOS = ""
BROWSER_NAME_CHROME = 'Chrome'
BROWSER_NAME_SAFARI = 'Safari'


# driver objects
# LOGIN_BUTTON = @driver.find_element(css:$css_login_button)

#fail default message
$fail_message = "Ooops, element not present"
$fail_companion_title = 'companion title not displayed'
$fail_title = 'title not displayed'
$fail_mr = 'mr not displayed'
$fail_mrs = 'mrs not displayed'
$fail_mss = 'mss not displayed'
$fail_name_displayed = 'name is not displayed'
$fail_surname_displayed = 'surname not displayed'
$fail_dob_displayed = 'dob not displayed'
$fail_nationality_displayed = 'country not displayed'
$fail_companion_displayed = 'companion not displayed'
$fail_family = 'family category not present'
$fail_friend = 'friend category not present'
$fail_colleague = 'colleague category not present'
$fail_companion_details_title = 'not companion details'
$fail_back_button = 'back button not present'
$fail_initials = 'initials not present'
$fail_nil_text = 'text equals to nil'
$fail_text_match = 'text does not match'

# elements login
$css_profile_icon = "[data-ref='nav-header-mobile__right-profile-selector']"
$css_email_field = "[data-ref='email_input']"
$css_password_field = "[data-ref='password_input']"
$css_login_button = "[data-ref='signup_login_cta']"

#login details
$email_with_data_login = "caheta@ryanair.com"
$email_empty_data_login ="testdub@12storage.com"
# EMAIL_WITHOUT_DATA_LOGIN = ""
$password_login = "Password1"

# profile icon logged in menu
$css_profile_dashboard_button = "div.right-side-menu__navigation > a:nth-child(1)"
$css_profile_myprofile_button = "div.right-side-menu__navigation > a:nth-child(2)"
$css_profile_travel_credit_button = "div.right-side-menu__navigation > a:nth-child(3)"
$css_profile_payment_methods_button = "div.right-side-menu__navigation > a:nth-child(4)"
$css_profile_companions_button = "div.right-side-menu__navigation > a:nth-child(5)"

# view companions empty state
$class_companions_empty_image = "empty-state__image"
$css_companions_empty_message = "label[class$='h2 empty-state__text']"

# view companions details
$css_companions_details_header = 'div.nhm > div > div.nhm__logo-container > span'
# $css_companions_card_arrow_icon = "[data-ref='companions-card-icon-right']"
$css_companions_card_initials = "[data-ref='companions-card-initials']"
$css_companions_card_info_name = "[data-ref='companions-card-info-name']"
$css_companions_card_travel_doc_info = "[data-ref='companions-card-info-name']"
$css_companions_card_type = "[data-ref='companions-card-type']"
$css_companions_add_new_button = "[data-ref='companions-add-button']"

# add new companions maxed out
$class_maxed_out_icon = "icon-40"
$class_maxed_out_title = "maxed-companions__dialog-header-text"
$class_maxed_out_message = "maxed-companions__dialog-body-text"
$css_maxed_out_button = "[data-ref='companions-limit-close-button']"

# add new companions
$css_add_companion_back_button = ""
$css_add_companion_title = "[data-ref='add-companion-form-title']"
$css_add_companion_title_MR = "div:nth-child(1) > select > option:nth-child(2)"
$css_add_companion_title_MRS = "div:nth-child(1) > select > option:nth-child(3)"
$css_add_companion_title_MSS = "div:nth-child(1) > select > option:nth-child(4)"
$css_add_companion_first_name = "[data-ref='add-companion-form-name']"
$css_add_companion_last_name = "[data-ref='add-companion-form-surname']"
$css_add_companion_date_of_birth = "[data-ref='add-companion-form-date-of-birth']"
$css_add_companion_country = "[data-ref='add-companion-form-country']"
$css_add_companion_companion_type = "[data-ref='add-companion-form-companion-type']"
$css_add_companion_companion_type_FAMILY = "div:nth-child(6) > select > option:nth-child(2)"
$css_add_companion_companion_type_COLLEAGUE = "div:nth-child(6) > select > option:nth-child(3)"
$css_add_companion_companion_type_FRIEND = "div:nth-child(6) > select > option:nth-child(4)"
$css_add_companion_save_button = "[data-ref='add-companion-form-save-button']"

# companion details
$css_companion_details_title = 'div.nhm > div > div.nhm__logo-container > span'
$css_companion_details_back_button = "[data-ref='nav-header-mobile__left-hamburger-selector']"
$css_companion_details_name = "[data-ref='companions-details-name']"
# $css_companion_details_dob_icon ="companion-details__dob-icon"
$css_companion_details_dob_icon_title = 'div:nth-child(2) > div.companion-details__property-field > div.companion-details__property-field--title.b3'
$css_companion_details_dob = "[data-ref='companions-details-dob']"
# $class_companion_details_nationality_icon = "companion-details__nationality-icon"
$css_companion_details_nationality_title= "div:nth-child(3) > div.companion-details__property-field > div.companion-details__property-field--title.b3"
$css_companion_details_nationality ="[data-ref='companions-details-nationality']"
# $css_companion_details_edit_icon = "companion-details__edit-icon"
$css_companion_details_edit_title = "div > div:nth-child(4) > div.companion-details__action-text.h4"
# $css_companion_details_edit_arrow ="div > div.companion-details__edit > icon > span"
# $class_companion_details_delete_icon = "companion-details__delete-icon"
$css_companion_details_delete_title = "[data-ref='companions-details-cta-delete']"
# $class_companion_details_delete_arrow = "div.companion-details__delete > icon > span"
$css_companion_details_travel_doc_title = "div.companion-details__documents-header"
# $css_companion_details_add_icon = "div.companion-details__add-icon > icon > span"
$css_companion_details_add_title ="[data-ref='companions-details-td-cta-add']"
# $css_companion_details_add_arrow= "div > div.companion-details__add > icon"

# add document travel
$id_document_icon = "Layer_1"
$class_document_name = "add-document__title--text"
$css_document_nationality = "[data-ref='add-document-form-nationality']"
$css_document_type = "[data-ref='add-document-form-document-type']"
$css_document_expiry_date = "[data-ref='add-document-form-expiry-date']"
$css_document_number = "[data-ref='add-document-form-document-number']"
$css_document_country_of_issue = "[data-ref='add-document-form-country']"


# delete companion view
$css_delete_title = "span.delete-companion__dialog-header-text.h2"
$css_delete_message = "span.delete-companion__dialog-body-text.b2"
$css_delete_button = "[data-ref='companions-delete-confirm-button']"
$css_delete_cancel = "[data-ref='companions-delete-cancel-button']"
