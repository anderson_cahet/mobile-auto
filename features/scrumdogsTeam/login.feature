
Feature: Login
  Background:
    Given I go to url
    And I select profile icon

  @login @r
  Scenario: Login with account that has companions and payments
    And I add valid credentials
    When I select login button
    Then I am in my profile menu
  @login
  Scenario: Login with account without companions and payments
    And I add credentials without payments and companions
    * I select login button
    * I am in my profile menu


