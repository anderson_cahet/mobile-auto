Feature: Companions with data
  Background:
    * I go to ryanair url
    * I select profile icon
    * I add valid credentials
    * I select login button
    * I am in my profile menu

  @companions @view @pass @l
  Scenario: MOBILE - Companions view
    When I select companions
    Then I see companions card as per visual


  @companions @pass
  Scenario:view Companion Details
    * I select companions
    When I select companion details
    Then I see companion details title
    And I see back button
    And I see companion initials
    And I see companion name
    And I see date of birth populated
    And I see nationality
    And I see edit companion details button
    And I see delete companion button
    And I see travel documents section
    And I see add travel document button

  @companions @max @pass
  Scenario: Maxed out message pop up displays
    * I select companions
    When I select add new companions button
    Then I see pop with maxed out message as per visuals

  @companions @pass
  Scenario: Delete companion View
    * I select companions
    * I select companion details
    * I select delete companions
    Then I see title
    And I see message
    And I select cancel button

  @companions @pass
  Scenario: Delete Companions
    * I select companions
    * I select companion details
    And I select delete companions
    When I confirm delete companion
    * I select add new companions button
    * I see name

  @companions @pass
  Scenario: View Edit companion
    * I select companions
    * I select companion details
    When I select edit companion
    Then I see all fields populated
