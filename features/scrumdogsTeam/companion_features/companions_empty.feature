Feature: Companions with no data
  Background:
    * I go to url
    * I select profile icon
    * I add credentials without payments and companions
    * I select login button
    * I am in my profile menu
    * I select companions

  @companions @empty @pass
  Scenario: View companions in empty state
    Then I see companions in empty state image
    And I see empty state message
    And I see add new companion button

  @companions @pass
  Scenario: Add companion view - Verify add companion view page elements
    * I select add new companions button
    Then I select title as dropdown
    And I see title options
    And I see name
    And I see surname
    And I see date of birth
    And I see nationality dropdown
    And I see companion category dropdown
    And companion category options
