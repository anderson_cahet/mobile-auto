File.read('constants.rb')
# require '././features/config/Common_Functions'
And(/^I select to add new travel document$/) do
  sleep(1)
  driver.touch.scroll(0, 400).perform
  driver.find_element(css: $css_companion_details_add_title).click
end

When(/^I select "([^"]*)" nationality$/) do |nationality|
  sleep(1)
  dropdown = driver.find_element(css:$css_document_nationality)
  Common_Functions::select_element_from_dropdown(dropdown, :text, nationality)
end

Then(/^I see "([^"]*)" "([^"]*)" documents in document type$/) do |doc1, doc2|
  Common_Functions::select_css_element($css_document_type)
  documents = driver.find_element(css:$css_document_type).text
  if doc1.nil? || doc2.empty?
    fail("Got: #{documents}, however Expected: #{doc1} #{doc2}") unless documents.include? doc1
  else
    fail("Got: #{documents}, however Expected: #{doc1} #{doc2}") unless documents.include? doc1 && doc2
  end
end
