File.read('constants.rb')
When(/^I select add new travel document$/) do
  sleep(1)
  driver.touch.scroll(0, 400).perform
  Common_Functions::select_css_element($css_companion_details_add_title)
end

Then(/^I see icon$/) do
  Common_Functions::find_id_element($id_document_icon)
end

And(/^I see companion name is present$/) do
  Common_Functions::find_class_element($class_document_name)
end

And(/^I see nationality dropdown is present$/) do
  Common_Functions::find_css_element($css_document_nationality)
end

And(/^I see document type$/) do
  Common_Functions::find_css_element($css_document_type)
end

And(/^I see expiry date$/) do
  Common_Functions::find_css_element($css_document_expiry_date)
end

And(/^I see document number$/) do
  Common_Functions::find_css_element($css_document_number)
end

And(/^I see country of issue$/) do
  Common_Functions::find_css_element($css_document_country_of_issue)
end
