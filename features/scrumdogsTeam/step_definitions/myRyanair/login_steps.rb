File.read('constants.rb')
require 'yaml'
require '././features/config/Common_Functions'
config=YAML.load_file("././features/config/secrets.yml")

Given(/^I go to url$/) do
  driver.get(config['scrumdogs'])
end

And(/^I select profile icon$/) do
  Common_Functions::select_css_element($css_profile_icon)
end

And(/^I add valid credentials$/) do
  Common_Functions.send_keys_to_css_element($css_email_field,$email_with_data_login)
  Common_Functions.send_keys_to_css_element($css_password_field,$password_login)
end

When(/^I select login button$/) do
  Common_Functions::select_css_element($css_login_button)
end

Then(/^I am in my profile menu$/) do
  Common_Functions::select_css_element($css_profile_icon)
  Common_Functions::find_css_element($css_profile_dashboard_button)
  Common_Functions::find_css_element($css_profile_myprofile_button)
  Common_Functions::find_css_element($css_profile_travel_credit_button)
  Common_Functions::find_css_element($css_profile_payment_methods_button)
  Common_Functions::find_css_element($css_profile_companions_button)
end

And(/^I add credentials without payments and companions$/) do
  Common_Functions.send_keys_to_css_element($css_email_field,$email_empty_data_login)
  Common_Functions.send_keys_to_css_element($css_password_field,$password_login)
end
