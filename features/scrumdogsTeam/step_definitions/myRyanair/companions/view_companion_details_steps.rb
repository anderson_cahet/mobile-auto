File.read('constants.rb')
require '././features/config/Common_Functions'
When(/^I select companion details$/) do
  Common_Functions::select_css_element($css_companions_card_info_name)
end

Then(/^I see companion details title$/) do
  Common_Functions::get_css_element_text($css_companions_details_header)
end

And(/^I see back button$/) do
  Common_Functions::find_css_element($css_companion_details_back_button)
end

And(/^I see companion initials$/) do
  Common_Functions::find_css_element($css_companions_card_initials)
end

And(/^I see companion name$/) do
  Common_Functions::find_css_element($css_companion_details_name)
end

And(/^I see date of birth populated$/) do
  Common_Functions::get_css_element_text($css_companion_details_dob_icon_title)
  Common_Functions::get_css_element_text($css_companion_details_dob)
end

And(/^I see nationality$/) do
  Common_Functions::get_css_element_text($css_companion_details_nationality_title)
  Common_Functions::get_css_element_text($css_companion_details_nationality)
end

And(/^I see edit companion details button$/) do
  Common_Functions::get_css_element_text($css_companion_details_edit_title)
end

And(/^I see delete companion button$/) do
  Common_Functions::get_css_element_text($css_companion_details_delete_title)
end

And(/^I see travel documents section$/) do
  Common_Functions::get_css_element_text($css_companion_details_travel_doc_title)
end

And(/^I see add travel document button$/) do
  driver.touch.scroll(0, 150).perform
  Common_Functions::get_css_element_text($css_companion_details_add_title)
end
