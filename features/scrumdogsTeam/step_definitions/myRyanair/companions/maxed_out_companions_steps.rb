File.read('constants.rb')

When(/^I select add new companions button$/) do
  sleep(2)
  companion_add_new = driver.find_element(css:$css_companions_add_new_button)
  !companion_add_new.displayed? ? fail('add button not present') : companion_add_new.click
end

Then(/^I see pop with maxed out message as per visuals$/) do
  Common_Functions::find_class_element($class_maxed_out_icon)
  Common_Functions::find_class_element($class_maxed_out_title)
  Common_Functions::find_class_element($class_maxed_out_message)
  Common_Functions::find_css_element($css_maxed_out_button)
end
