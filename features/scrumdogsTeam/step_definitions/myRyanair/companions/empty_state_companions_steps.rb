File.read('constants.rb')
Then(/^I see companions in empty state image/) do
  Common_Functions::find_class_element($class_companions_empty_image)
end

And(/^I see empty state message/) do
  Common_Functions::find_css_element($css_companions_empty_message)
end

And(/^I see add new companion button$/) do
  Common_Functions::find_css_element($css_companions_add_new_button)
end
