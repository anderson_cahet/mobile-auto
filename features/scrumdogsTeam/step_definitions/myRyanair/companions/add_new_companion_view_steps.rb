File.read('constants.rb')
Then(/^I select title as dropdown$/) do
  Common_Functions::select_css_element($css_add_companion_title)
end

And(/^I see title options$/) do
  Common_Functions::find_css_element($css_add_companion_title_MR)
  Common_Functions::find_css_element($css_add_companion_title_MRS)
  Common_Functions::find_css_element($css_add_companion_title_MSS)
  Common_Functions::select_css_element($css_add_companion_title)
end

And(/^I see name$/) do
  Common_Functions::find_css_element($css_add_companion_first_name)
end

And(/^I see surname$/) do
  Common_Functions::find_css_element($css_add_companion_last_name)
end

And(/^I see date of birth$/) do
  Common_Functions::find_css_element($css_add_companion_date_of_birth)
end

And(/^I see nationality dropdown$/) do
  Common_Functions::select_css_element($css_add_companion_country)
end

And(/^I see companion category dropdown$/) do
  Common_Functions::select_css_element($css_add_companion_companion_type)
end

And(/^companion category options$/) do
  Common_Functions::find_css_element($css_add_companion_companion_type)
  Common_Functions::find_css_element($css_add_companion_companion_type_FAMILY)
  Common_Functions::find_css_element($css_add_companion_companion_type_FRIEND)
  Common_Functions::find_css_element($css_add_companion_companion_type_COLLEAGUE)
  Common_Functions::select_css_element($css_add_companion_companion_type)
end
