File.read('constants.rb')

When(/^I select companions$/) do
  Common_Functions::select_css_element($css_profile_companions_button)
end

Then(/^I see companions card as per visual$/) do
  Common_Functions::find_css_element($css_companions_card_initials)
  Common_Functions::find_css_element($css_companions_card_info_name)
  Common_Functions::find_css_element($css_companions_card_type)
  Common_Functions::find_css_element($css_companions_card_travel_doc_info)
  Common_Functions::find_css_element($css_companions_add_new_button)
end
