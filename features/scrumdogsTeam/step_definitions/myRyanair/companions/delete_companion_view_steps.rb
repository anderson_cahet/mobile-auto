File.read('constants.rb')
require '././features/config/Common_Functions'
Then(/^I see title$/) do
  Common_Functions.get_css_element_text($css_delete_title)
end

And(/^I see message$/) do
  Common_Functions.get_css_element_text($css_delete_title)
end

And(/^I select cancel button$/) do
  Common_Functions.select_css_element($css_delete_cancel)
end
