File.read('constants.rb')
require '././features/config/Common_Functions'
And(/^I select delete companions$/) do
  Common_Functions::select_css_element($css_companion_details_delete_title)
end

When(/^I confirm delete companion$/) do
  Common_Functions::select_css_element($css_delete_button)
end
