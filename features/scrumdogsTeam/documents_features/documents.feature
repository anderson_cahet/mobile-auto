Feature: Documents
  Background:
    * I go to url
    * I select profile icon
    * I add valid credentials
    * I select login button
    * I am in my profile menu
    * I select companions
    * I select companion details

  @documents @pass
  Scenario Outline: Document displayed in document type after nationality selected
    And I select to add new travel document
    When I select "<nationality>" nationality
    Then I see "<document1>" "<document2>" documents in document type

    Examples:
      | nationality | document1 | document2            |
      | Andorran    | Passport  |                      |
      | Irish       | Passport  | Passport Card        |
      | Italian     | Passport  | EEA National ID Card |
      | Brazilian   | Passport  |                      |

  @documents @pass
  Scenario: Travel document default view
    When I select add new travel document
    Then I see icon
    And I see companion name is present
    And I see nationality dropdown is present
    And I see document type
    And I see expiry date
    And I see document number
    And I see country of issue
