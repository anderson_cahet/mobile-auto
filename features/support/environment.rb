require 'appium_lib'
require 'yaml'
$config=YAML.load_file("././features/config/secrets.yml")

def android
  { caps:
        {
            :deviceName => $config['androidDeviceName'],
            :browserName => $config['androidBrowserName'],
            :appPackage => $config['androidAppPackage'],
            :platformName => $config['androidPlatformName'],
            :automationName => $config['androidAutomationName'],
        }
  }
end
def ios
  { caps: {
      :udid => $config['iphoneUDID'],
      :deviceName => $config['deviceName'],
      :browserName => $config['Safari'],
      :automationName => $config['automationName'],
      :platformVersion => $config['platformVersion'],
      :platformName => $config['platformName'],
      :bundleId => $config['com.apple.mobilesafari'],
      :xcodeOrgId => $config['xcodeOrgId'],
      :xcodeSigningId => $config['iphoneUDID'],
      :agentPath => $config['agentPath'],
      :bootstrapPath => $config['bootstrapPath'],
    }
  }
end
@driver = Appium::Driver.new(android, true)
Appium.promote_appium_methods Object

