File.read('constants.rb')
Before do
  # appium = Runtime.getRunTime.exec

  $driver.start_driver
  puts('<----before hook is executed before every scenario:----> ')
end

After do |scenario|

  if scenario.failed?
    FileUtils.mkdir_p('screenshots') unless File.directory?('screenshots')

    time_failure = Time.now.strftime('Time: %H:%M.%S Date: %d_%m-%Y')
    screenshot_name = "#{time_failure}.png"
    screenshot_file = File.join('screenshots', screenshot_name)
    $driver.screenshot(screenshot_file)
    embed("#{screenshot_file}", 'image/png')
  end
  sleep 3
  $driver.driver_quit
  puts('<----After hook is executed after every scenario---->')
end

AfterConfiguration do
  if File.directory?("screenshots")
    FileUtils.rm_r("screenshots")
  end
end