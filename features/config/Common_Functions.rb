module Common_Functions
  def Common_Functions.wait_to_click_element(click_element)
    wait = Selenium::WebDriver::Wait.new(:timeout => 25)
    wait.until do
      element = click_element
      1.times { element.click }
    end
  end

  def Common_Functions.select_element_from_dropdown(element_to_be_selected, how, what)
    drop = Selenium::WebDriver::Support::Select.new(element_to_be_selected)
    drop.select_by(how, what)
  end

  def Common_Functions.is_element_present(how, what)
    driver.manage.timeouts.implicit_wait = 0
    result = driver.find_elements(how, what).size() > 0
    if result
      result = driver.find_element(how, what).displayed?
    end
    driver.manage.timeouts.implicit_wait = 30
    return result
  end

  def Common_Functions.select_css_element(what)
    find = driver.find_element(css: what)
    !find.displayed? ? fail($fail_message) : return wait_to_click_element(find)
  end

  def Common_Functions.select_class_element(what)
    find = driver.find_element(class: what)
    (!find.displayed? || find.nil?) ? fail($fail_message) : return wait_to_click_element(find)
  end

  def Common_Functions.find_css_element(what)
    find = driver.find_element(css: what)
    (!find.displayed? || find.nil?) ? fail($fail_message) : return find
  end

  def Common_Functions.find_class_element(what)
    find = driver.find_element(class: what)
    (!find.displayed? || find.nil?) ? fail($fail_message) : return find
  end
  def Common_Functions.find_id_element(what)
    find = driver.find_element(id: what)
    (!find.displayed? || find.nil?) ? fail($fail_message) : return find
  end

  def Common_Functions.send_keys_to_css_element(element, keys_to_be_sent)
    sleep(3)
    find = find_css_element(element)
    result = find.send_keys(keys_to_be_sent)
    (!find.displayed? || find.nil?) ? fail($fail_message) : return result
  end

  def Common_Functions.get_css_element_text(what)
    find = driver.find_element(css: what).text
    fail($fail_nil_text) if find.nil? || find.empty?
  end
end
